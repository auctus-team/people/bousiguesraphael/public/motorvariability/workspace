import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(10,10))

theta_y_lim_df=pd.read_csv('theta_y_lim_15_trustconst.csv',header=0)

print(theta_y_lim_df)


plt.rc('xtick', labelsize=20) 
plt.rc('ytick', labelsize=20) 

plt.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_min']*180 / np.pi,color='green')
plt.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_max']*180 / np.pi,color='green')
plt.fill_between(theta_y_lim_df['s'],theta_y_lim_df['theta_y_max']*180 / np.pi,theta_y_lim_df['theta_y_min']*180 / np.pi)
plt.xlabel('s',fontsize=22)
plt.ylabel('theta_y [°]',fontsize=22)
# plt.title('Theta_y angle bounds for path n°'+str(n_path),fontsize=22)


# plt.legend(fontsize='15', title_fontsize='40')
plt.show()