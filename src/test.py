import robotics_mec as rob
import matplotlib.pyplot as plt
from numpy import pi
from roboticstoolbox import *
from spatialmath import *

panda = models.DH.Panda()
panda.tool  = SE3.Tz(0.093)  # set no tool at the end !

q = [0, 0, 0,0,0, 0, 0]

M_rt = panda.fkine(q)
M_rm = rob.FK_panda(q)

print("Robotics Toolbox=\n",M_rt)
print("Robotics Mec=\n",M_rm)
print("Robotics DH=\n",panda)
print("RTB path=\n",panda.fkine_all(q, old=False))

# panda.plot(q, block=True)


