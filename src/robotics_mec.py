import numpy as np
from numpy import array,pi,cos, sin, matmul

def Mi_1i_craigs(d = 0.0, theta = 0.0, a = 0.0, alpha = 0.0):
    print("\nd=",d)
    print("theta=",theta)
    print("a=",a)
    print("alpha=",alpha)
    return array([[cos(theta), -sin(theta), 0 , a],
            [sin(theta)*cos(alpha), cos(theta)*cos(alpha), -sin(alpha), -d*sin(alpha)],
            [sin(alpha)*sin(theta),cos(theta)*sin(alpha),cos(alpha), d*cos(alpha)],
            [0,0,0,1]])

def FK_panda(q = array([0, 0, 0, 0, 0, 0, 0])):
    # define DH parameters in Craigs convention for 7dofs
    a = array([0,0,0,0.0825,-0.0825,0,0.088,0])
    d = array([0.333,0,0.316,0,0.384,0,0,0.107])
    alpha = array([0,-pi/2,pi/2,pi/2,-pi/2,pi/2,pi/2,0])
    theta8 = 0
    theta = np.concatenate([q,[theta8]])

    # Define matrices
    M01 = Mi_1i_craigs(d[0],theta[0],a[0],alpha[0])
    M12 = Mi_1i_craigs(d[1],theta[1],a[1],alpha[1])
    M23 = Mi_1i_craigs(d[2],theta[2],a[2],alpha[2])
    M34 = Mi_1i_craigs(d[3],theta[3],a[3],alpha[3])
    M45 = Mi_1i_craigs(d[4],theta[4],a[4],alpha[4])
    M56 = Mi_1i_craigs(d[5],theta[5],a[5],alpha[5])
    M67 = Mi_1i_craigs(d[6],theta[6],a[6],alpha[6])
    M78 = Mi_1i_craigs(d[7],theta[7],a[7],alpha[7])

    M02 = matmul(M01,M12)
    M03 = matmul(M02,M23)
    M04 = matmul(M03,M34)
    M05 = matmul(M04,M45)
    M06 = matmul(M05,M56)
    M07 = matmul(M06,M67)
    M08 = matmul(M07,M78)
    print("M01=\n",np.round(M01, 4))
    print("M02=\n",np.round(M02, 4))
    print("M03=\n",np.round(M03, 4))
    print("M04=\n",np.round(M04, 4))
    print("M05=\n",np.round(M05, 4))
    print("M06=\n",np.round(M06, 4))
    print("M07=\n",np.round(M07, 4))
    print("M08=\n",np.round(M08, 4))


    return np.round(M08, 4)

def Q_lim_panda():
    return array([[-2.8973, -1.7628, -2.8973, -3.0718, -2.8973, -0.0175, -2.8973][2.8973, 1.7628, 2.8973, -0.0698, 2.8973, 3.7525, 2.8973]])

#print("M08=",FK_panda())