# Workspace

This project contains many tools used to design the path (or wireloop) according to robot workspace. 

To avoid compatibility issue, please use at least [Python 3.9](https://www.python.org/downloads/release/python-390/).

- Use _Path.py_ to design the path 
- Use _Workspace.py_ to compute the maximum amplitude of rotation of the handle around the designed path.
- Use _robotics_mec.py_ to compute FK for the Franka Panda robot. 

## Path.py

This program is used to design a path in space in many step:
1. The definition of the shape of the path.

The current design is configured to compute a 2D path, using a reference abscissa $`s`$, and 15 parameters

```math
\left \{ \begin{array}{l}
x_t(s) = 0 \\
y_t(s) = s \\
z_t(s) = A(dA+s)+B \sin(f_1 s)+C \cos(f_1 s)+D \sin(f_2s)^2+E \cos(f_2s)^2+F \sin(f_3 s)^3+G \cos(f_3 s)^3 + H(dH+s)^2 + I(dI+s)^3
\end{array} \right .
```

   - $`A`$  -   the gradient of the function
   - $`dA`$   -   the offset of the reference abscissa for the gradient term
   - $`B`$ - the amplitude of sine
   - $`C`$ - the amplitude of cosine
   - $`D`$ - the amplitude of squared sine
   - $`E`$ - the amplitude of squared cosine
   - $`F`$ - the amplitude of cubic sine
   - $`G`$ - the amplitude of cubic cosine
   - $`H`$ - the gradient of squared function
   - $`dH`$ - the offset of the reference abscissa for the squared function
   - $`I`$ - the gradient of cubic function
   - $`dI`$ - the offset of the reference abscissa for the cubic function
   - $`f_1`$ - the pulsation for sine and cosine (in rad/s)
   - $`f_2`$ - the pulsation for squared sine and cosine (in rad/s)
   - $`f_3`$ - the pulsation for cubic sine and cosine (in rad/s)

_Note: You can see that in fact, $`x_t(s) = 1e-5s`$ in the code. This value corresponds to a very low value and limits true zeros, to avoid divisions by 0 in the document._

**Warning**: The program needs to know the maximum and minimum values of each axis. This data are automatically computed on X- and Y-axis, but not on Z-axis. Please, don't forgot to change the $`Z\_t\_max`$ and $`Z\_t\_min`$ values !

2. The position of the path in space

The position of the path is only defined by the position of the first point $`s=0`$ with:

   - $`X\_start`$ - the X coordinate of first point
   - $`Y\_start`$ - the Y coordinate of first point
   - $`Z\_start`$ - the Z coordinate of first point

3. The size of the path in space

The size of the path is defined by 3 parameters :

   - $`depth`$ - the difference between $`X\_t\_max`$ and $`X\_t\_min`$, so the maximal amplitude of the path along X-axis
   - $`width`$ - the difference between $`Y\_t\_max`$ and $`Y\_t\_min`$, so the maximal amplitude of the path along Y-axis
   - $`height`$ - the difference between $`Z\_t\_max`$ and $`Z\_t\_min`$, so the maximal amplitude of the path along Z-axis

4. The real path, sent to the Panda, is computed by:

```math
\left \{ \begin{array}{l}
x_r(s) = \frac{x_t(s)-x_t(0)}{X\_t\_max - X\_t\_min} * depth + X\_start   \\
y_r(s) = \frac{y_t(s)-y_t(0)}{Y\_t\_max - Y\_t\_min} * width + Y\_start \\
z_r(s) = \frac{z_t(s)-z_t(0)}{Z\_t\_max - Z\_t\_min} * height + Z\_start
\end{array} \right .
```
_Note: You can see in the code that the value $`1e-10s`$ is added to $`x_r(s)`$. This value corresponds to a very low value and limits true zeros, to avoid divisions by 0 in the document._

The code returns in the terminal window the analytical expressions of the path, its, derivative, and its second derivative for each coordinates.

The function _PathFrame(s)_ returns the control frame for the robot on a given reference abscissa along the path, through a transformation matrix expressed in world frame. The frame is configured as explained in [this Latex document](https://www.overleaf.com/read/djtskrxnchpm), part VI. 

## Workspace.py

This program needs the  [_roboticstoolbox_](https://github.com/petercorke/robotics-toolbox-python) and the [_spatialmath_](https://github.com/petercorke/spatialmath-python) libraries from Peter Corke. It also needs the program _Path.py_ defined above.



## Robotics_mec

This program uses the DH convention, with Craigs modified parameters. It returns the FK of the Panda robot using the _FK_panda(q)_ function and the robot bounds using the _Q_lim_panda()_ function. The function _Mi_1i_craigs(d, theta, a, alpha)_ return the transformation matrix between two successive links using the Craig's convention.

